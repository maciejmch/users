//
//  UsersListsCells.swift
//  Users
//
//  Created by Maciej Chmielewski on 10/12/2018.
//  Copyright © 2018 Maciej Chmielewski. All rights reserved.
//

import UIKit
import AlamofireImage

class AlertListCell: UITableViewCell {}

class UserListCell: UITableViewCell {
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var sourceIconImageView: UIImageView!
    
    override func awakeFromNib() {
        avatarImageView?.layer.cornerRadius = 27
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        avatarImageView.image = nil
        avatarImageView.af_cancelImageRequest()
    }
}
