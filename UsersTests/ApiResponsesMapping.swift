//
//  ApiResponsesMapping.swift
//  UsersTests
//
//  Created by Maciej Chmielewski on 10/12/2018.
//  Copyright © 2018 Maciej Chmielewski. All rights reserved.
//

import Foundation
import XCTest
import Nimble
@testable import Users

class ApiResponsesMappingTests: XCTestCase {
    func testGitHubUserMapping() {
        let data = try! Data(contentsOf: Bundle(for: ApiResponsesMappingTests.self).url(forResource: "SampleResponses/github_user", withExtension: "json")!)
        let decodingResult: Failable<GitHubUser> = ApiResponseJsonDecoder().decode(data: data)
        expect(decodingResult.optional).toNot(beNil())
    }
    
    func testDailyMotionUserMapping() {
        let data = try! Data(contentsOf: Bundle(for: ApiResponsesMappingTests.self).url(forResource: "SampleResponses/dailymotion_user", withExtension: "json")!)
        let decodingResult: Failable<DailyMotionUser> = ApiResponseJsonDecoder().decode(data: data)
        expect(decodingResult.optional).toNot(beNil())
    }
}
