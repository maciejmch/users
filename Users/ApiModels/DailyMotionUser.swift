//
//  DailyMotionUser.swift
//  Users
//
//  Created by Maciej Chmielewski on 10/12/2018.
//  Copyright © 2018 Maciej Chmielewski. All rights reserved.
//

import Foundation

struct DailyMotionUser: Decodable {
    let name: String
    let avatarUrl: URL
    
    enum CodingKeys: String, CodingKey {
        case name = "username"
        case avatarUrl = "avatar_360_url"
    }
}
