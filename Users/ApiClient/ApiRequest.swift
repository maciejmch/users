//
//  ApiRequest.swift
//  Users
//
//  Created by Maciej Chmielewski on 10/12/2018.
//  Copyright © 2018 Maciej Chmielewski. All rights reserved.
//

import Foundation
import Alamofire

struct ApiRequest {
    let method: HTTPMethod
    let url: URL
    let parameters: Parameters?
    let parametersEncoding: ParameterEncoding
    let headers: [String: String]?
    
    init(method: HTTPMethod = .get, url: URL, parameters: Parameters? = nil, parametersEncoding: ParameterEncoding = URLEncoding.default, headers: [String: String]? = nil) {
        self.method = method
        self.url = url
        self.parameters = parameters
        self.parametersEncoding = parametersEncoding
        self.headers = headers
    }
}
