//
//  UsersListViewModel.swift
//  Users
//
//  Created by Maciej Chmielewski on 10/12/2018.
//  Copyright © 2018 Maciej Chmielewski. All rights reserved.
//

import UIKit
import AlamofireImage

fileprivate struct UsersListAppearence {
    static let cellHeight: CGFloat = 78
}

protocol ListCellModel {
    func cellGetter(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell
    func cellHeightGetter(tableView: UITableView) -> CGFloat
}

struct LoadingPlaceholderListCellModel: ListCellModel {
    func cellGetter(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        return tableView.dequeueReusableCell(withIdentifier: "LoadingPlaceholderListCell") as! LoadingPlaceholderListCell
    }
    
    func cellHeightGetter(tableView: UITableView) -> CGFloat {
        return UsersListAppearence.cellHeight
    }
    
}

struct AlertListCellModel: ListCellModel {
    let refresh: () -> Void
    
    func cellGetter(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        return tableView.dequeueReusableCell(withIdentifier: "AlertListCell") as! AlertListCell
    }
    
    func cellHeightGetter(tableView: UITableView) -> CGFloat {
        return tableView.frame.height
    }
}

struct UserListCellModel: ListCellModel {
    let name: String
    let avatarUrl: URL
    let sourceImage: UIImage
    let select: () -> Void
    
    func cellGetter(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "UserListCell") as! UserListCell
        cell.nameLabel.text = name
        cell.avatarImageView.af_setImage(withURL: avatarUrl)
        cell.sourceIconImageView.image = sourceImage
        return cell
    }
    
    func cellHeightGetter(tableView: UITableView) -> CGFloat {
        return UsersListAppearence.cellHeight
    }
}
