//
//  InterfaceBuilderInstance.swift
//  Users
//
//  Created by Maciej Chmielewski on 10/12/2018.
//  Copyright © 2018 Maciej Chmielewski. All rights reserved.
//
import UIKit

protocol InterfaceBuilderInstance {
    static var interfaceBuilderFileName: String {get}
}

extension InterfaceBuilderInstance where Self: UIViewController {
    static func make() -> Self {
        return UIStoryboard(name: Self.interfaceBuilderFileName, bundle: nil).instantiateInitialViewController() as! Self
    }
}

// MARK: Concretes
extension UsersListViewController: InterfaceBuilderInstance {static let interfaceBuilderFileName = "UsersList"}
extension UserDetailsViewController: InterfaceBuilderInstance {static let interfaceBuilderFileName = "UserDetails"}
