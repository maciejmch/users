//
//  UsersListViewController.swift
//  Users
//
//  Created by Maciej Chmielewski on 10/12/2018.
//  Copyright © 2018 Maciej Chmielewski. All rights reserved.
//

import UIKit

class UsersListViewController: UITableViewController {
    private var cellModels: [ListCellModel] = []
    
    func cellModelsDidUpdate(_ cellModels: [ListCellModel]) {
        self.cellModels = cellModels
        tableView.reloadData()
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cellModels.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = cellModels[indexPath.row].cellGetter(tableView: tableView, indexPath: indexPath)
        cell.backgroundColor = indexPath.row % 2 == 0 ? #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0) : #colorLiteral(red: 0.968627451, green: 0.968627451, blue: 0.968627451, alpha: 1)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return cellModels[indexPath.row].cellHeightGetter(tableView: tableView)
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cellModel = cellModels[indexPath.row]
        
        if let userCellModel = cellModel as? UserListCellModel {
            userCellModel.select()
        }
        
        if let alertCellModel = cellModel as? AlertListCellModel {
            alertCellModel.refresh()
        }
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        (cell as? LoadingPlaceholderListCell)?.startAnimating()
    }
    
    override func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        (cell as? LoadingPlaceholderListCell)?.stopAnimating()
    }
}
