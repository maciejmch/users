//
//  HttpResourcesTests.swift
//  UsersTests
//
//  Created by Maciej Chmielewski on 10/12/2018.
//  Copyright © 2018 Maciej Chmielewski. All rights reserved.
//

import Foundation

import Foundation
@testable import Users
import Nimble
import XCTest

class HttpResourcesTests: XCTestCase {
    func testGitHubUsersResource() {
        let expectServerResponse = XCTestExpectation(description: "expecting resource load")
        
        HttpResources.githubUsers.load { result in
            expect(result.optional).toNot(beNil())
            expectServerResponse.fulfill()
        }
        
        wait(for: [expectServerResponse], timeout: 10)
    }
    
    func testDailyMotionUserResource() {
        let expectServerResponse = XCTestExpectation(description: "expecting resource load")
        
        HttpResources.dailyMotionUsers.load { result in
            expect(result.optional).toNot(beNil())
            expectServerResponse.fulfill()
        }
        
        wait(for: [expectServerResponse], timeout: 10)
    }
}
