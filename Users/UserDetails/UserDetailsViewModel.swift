//
//  UserDetailsViewModel.swift
//  Users
//
//  Created by Maciej Chmielewski on 10/12/2018.
//  Copyright © 2018 Maciej Chmielewski. All rights reserved.
//

import UIKit

struct UserDetailsViewModel {
    let name: String
    let avatarUrl: URL
    let iconImage: UIImage
    let mainColor: UIColor
}

extension UserDetailsViewModel {
    static func fromGitHubUser(_ gitHubUser: GitHubUser) -> UserDetailsViewModel {
        return UserDetailsViewModel(name: gitHubUser.name, avatarUrl: gitHubUser.avatarUrl, iconImage: #imageLiteral(resourceName: "GutHub Icon Details"), mainColor: #colorLiteral(red: 0.5568627451, green: 0.8352941176, blue: 0.8431372549, alpha: 1))
    }
    
    static func fromDailyMotionUser(_ dailyMotionUser: DailyMotionUser) -> UserDetailsViewModel {
        return UserDetailsViewModel(name: dailyMotionUser.name, avatarUrl: dailyMotionUser.avatarUrl, iconImage: #imageLiteral(resourceName: "DailyMotion Icon Details"), mainColor: #colorLiteral(red: 0.568627451, green: 0.7098039216, blue: 0.8980392157, alpha: 1))
    }
}
