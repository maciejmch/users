//
//  ApiResponse.swift
//  Users
//
//  Created by Maciej Chmielewski on 10/12/2018.
//  Copyright © 2018 Maciej Chmielewski. All rights reserved.
//

import Foundation

struct ApiResponseJsonDecoder {
    func decode<Response: Decodable>(data: Data) -> Failable<Response> {
        let decoder = JSONDecoder()
        do {
            let decoded = try decoder.decode(Response.self, from: data)
            return .success(decoded)
        } catch (let error) {
            return .failure(error)
        }
    }
}
