//
//  ApiClientTests.swift
//  UsersTests
//
//  Created by Maciej Chmielewski on 10/12/2018.
//  Copyright © 2018 Maciej Chmielewski. All rights reserved.
//

import Foundation
@testable import Users
import Nimble
import XCTest

class ApiClientTests: XCTestCase {
    func testAnyDataResponse() {
        let expectServerResponse = XCTestExpectation(description: "expecting server response")
        let anyRequest = ApiRequest(url: URL(string: "https://www.google.com")!)
        ApiClient().requestData(anyRequest) {_ in
            expectServerResponse.fulfill()
        }
        wait(for: [expectServerResponse], timeout: 10)
    }
}
