//
//  InternetReconnectHandler.swift
//  Users
//
//  Created by Maciej Chmielewski on 10/12/2018.
//  Copyright © 2018 Maciej Chmielewski. All rights reserved.
//

import Foundation
import Alamofire

protocol InternetReconnectHandler: class {
    var onReconnect: (() -> Void)? {get set}
}

class InternetReconnectHandlerImpl: InternetReconnectHandler {
    let networkReachabilityManager = NetworkReachabilityManager()
    
    init() {
        networkReachabilityManager?.startListening()
        networkReachabilityManager?.listener = { [weak self] networkReachability in
            if case .reachable = networkReachability {
                
                self?.onReconnect?()
            }
        }
    }
    
    var onReconnect: (() -> Void)?
}
