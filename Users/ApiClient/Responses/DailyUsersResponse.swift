//
//  DailyUsersResponse.swift
//  Users
//
//  Created by Maciej Chmielewski on 10/12/2018.
//  Copyright © 2018 Maciej Chmielewski. All rights reserved.
//

import Foundation

struct DailyMotionUsersResponse: Decodable {
    let users: [DailyMotionUser]
    
    enum CodingKeys: String, CodingKey {
        case users = "list"
    }
}
