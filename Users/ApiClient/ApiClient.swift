//
//  ApiClient.swift
//  Users
//
//  Created by Maciej Chmielewski on 10/12/2018.
//  Copyright © 2018 Maciej Chmielewski. All rights reserved.
//

import Foundation
import Alamofire

struct ApiClient {
    func requestData(_ request: ApiRequest, completion: @escaping (Failable<Data>) -> Void) {
        Alamofire.request(request.url, method: request.method, parameters: request.parameters, encoding: request.parametersEncoding, headers: request.headers).responseData { dataResponse in
            switch dataResponse.result {
            case .success(let data): completion(.success(data))
            case .failure(let error): completion(.failure(error))
            }
        }
    }
}

extension ApiClient {
    func request<Response: Decodable>(_ request: ApiRequest, completion: @escaping (Failable<Response>) -> Void) {
        requestData(request) { dataResponse in
            switch dataResponse {
            case .success(let data): completion(ApiResponseJsonDecoder().decode(data: data))
            case .failure(let error): completion(.failure(error))
            }
        }
    }
}
