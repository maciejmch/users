//
//  AppCoordinator.swift
//  Users
//
//  Created by Maciej Chmielewski on 10/12/2018.
//  Copyright © 2018 Maciej Chmielewski. All rights reserved.
//

import UIKit

class AppCoordinator {
    private let window: UIWindow
    
    init(window: UIWindow) {
        self.window = window
    }
    
    func startAppFlow() {
        navigateToUsersList()
    }
    
    private lazy var usersListInteractor: UsersListInteractor = {
        return UsersListInteractor(gitHubUsersResource: HttpResources.githubUsers,
                                   dailyMotionUsersResource: HttpResources.dailyMotionUsers,
                                   internetReconnectHandler: InternetReconnectHandlerImpl())
    }()
    
    private lazy var userDetailsInteractor = UserDetailsInteractor()
    
    private func navigateToUsersList() {
        let usersListViewController = UsersListViewController.make()
        usersListInteractor.onCellModelsRefresh = { [weak usersListViewController] refreshedCellModels in
            usersListViewController?.cellModelsDidUpdate(refreshedCellModels)
        }
        presentAsRootController(usersListViewController)
        
        usersListInteractor.handleRoute = { [weak self, weak usersListViewController] route in
            guard let usersListViewController = usersListViewController else {return}
            switch route {
            case .showGitHubUserDetails(let gitHubUser): self?.navigateToGitHubUserDetails(gitHubUser, from: usersListViewController)
            case .showDailyMotionUserDetails(let dailyMotionUser): self?.navigateToDailyMotionUserDetails(dailyMotionUser, from: usersListViewController)
            }
        }
        usersListInteractor.refreshData()
    }
    
    private func navigateToGitHubUserDetails(_ gitHubUser: GitHubUser, from rootController: UIViewController) {
        navigateToUserDetails(UserDetailsViewModel.fromGitHubUser(gitHubUser), from: rootController)
    }
    
    private func navigateToDailyMotionUserDetails(_ dailyMotionUser: DailyMotionUser, from rootController: UIViewController) {
        navigateToUserDetails(UserDetailsViewModel.fromDailyMotionUser(dailyMotionUser), from: rootController)
    }
    
    private func navigateToUserDetails(_ viewModel: UserDetailsViewModel, from rootController: UIViewController) {
        let controller = UserDetailsViewController.make()
        controller.viewModel = viewModel
        presentAsDetailsController(controller, from: rootController)
        
        userDetailsInteractor.onClose = { [weak controller] in
            controller?.dismiss(animated: true, completion: nil)
        }
        controller.onClose = {[weak self] in
            self?.userDetailsInteractor.onClose?()
        }
    }
}

extension AppCoordinator {
    private func presentAsRootController(_ controller: UIViewController) {
        window.rootViewController = controller
        window.makeKeyAndVisible()
    }
    
    private func presentAsDetailsController(_ controller: UIViewController, from rootController: UIViewController) {
        rootController.present(controller, animated: true, completion: nil)
    }
}
