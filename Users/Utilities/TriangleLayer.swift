//
//  TriangleLayer.swift
//  Users
//
//  Created by Maciej Chmielewski on 10/12/2018.
//  Copyright © 2018 Maciej Chmielewski. All rights reserved.
//

import UIKit

struct TriangleLayer {
    func addLayerToView(_ view: UIView, color: UIColor) -> CALayer {
        let layer = CAShapeLayer()
        
        let path = UIBezierPath()
        path.move(to: CGPoint(x: 0, y: view.frame.height))
        path.addLine(to: CGPoint(x: view.frame.width, y: view.frame.height))
        path.addLine(to: CGPoint(x: view.frame.width * 0.5, y: 0))
        path.fill()
        
        layer.path = path.cgPath
        layer.fillColor = color.cgColor
            
        view.layer.addSublayer(layer)
        return layer
    }
}
