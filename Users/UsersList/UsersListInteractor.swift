//
//  UsersListInteractor.swift
//  Users
//
//  Created by Maciej Chmielewski on 10/12/2018.
//  Copyright © 2018 Maciej Chmielewski. All rights reserved.
//

import Foundation

extension UsersListInteractor {
    enum DataState {
        case initial
        case loading([LoadingPlaceholderListCellModel])
        case loaded([UserListCellModel])
        case oneOfSourcesFailedToLoad([UserListCellModel])
        case failedToLoad(AlertListCellModel)
    }
}

class UsersListInteractor {
    private(set) var dataState: DataState = .initial
    
    private let gitHubUsersResource: Resource<[GitHubUser]>
    private let dailyMotionUsersResource: Resource<DailyMotionUsersResponse>
    private let internetReconnectHandler: InternetReconnectHandler
    
    var onCellModelsRefresh: (([ListCellModel]) -> Void)?
    var handleRoute: ((Route) -> Void)?
    
    init(gitHubUsersResource: Resource<[GitHubUser]>, dailyMotionUsersResource: Resource<DailyMotionUsersResponse>, internetReconnectHandler: InternetReconnectHandler) {
        self.gitHubUsersResource = gitHubUsersResource
        self.dailyMotionUsersResource = dailyMotionUsersResource
        self.internetReconnectHandler = internetReconnectHandler
        
        setupInternetConnectionhandler()
    }
    
    func refreshData() {
        switch dataState {
        case .initial, .failedToLoad: changeStateToLoading()
        default: break
        }
        
        self.downloadData()
    }
    
    private func setupInternetConnectionhandler() {
        internetReconnectHandler.onReconnect = { [weak self] in
            self?.refreshData()
        }
    }
    
    private func downloadData() {
        var gitHubUsersDownloadResult: Failable<[GitHubUser]>?
        var dailyMotionUsersDownloadResult: Failable<DailyMotionUsersResponse>?
        
        let attemptToCompleteRefresh: () -> Void = {[weak self] in
            guard let `self` = self else {return}
            guard let gitHubUsersDownloadResult = gitHubUsersDownloadResult, let dailyMotionUsersDownloadResult = dailyMotionUsersDownloadResult else {return}
            
            switch (gitHubUsersDownloadResult, dailyMotionUsersDownloadResult) {
            case (.success(let gitHubUsers), .success(let dailyMotionUsers)): self.changeStateToLoaded(gitHubUsers: gitHubUsers, dailyMotionUsers: dailyMotionUsers.users)
            case (.failure, .failure): self.changeStateToFailedToLoad()
            case (.success(let gitHubUsers), .failure): self.changeStateToOneOfSourcesFailedToLoad(gitHubUsers: gitHubUsers, dailyMotionUsers: nil)
            case (.failure, .success(let dailyMotionUsers)): self.changeStateToOneOfSourcesFailedToLoad(gitHubUsers: nil, dailyMotionUsers: dailyMotionUsers.users)
            }
        }
        
        gitHubUsersResource.load { result in
            gitHubUsersDownloadResult = result
            attemptToCompleteRefresh()
        }
        
        dailyMotionUsersResource.load { result in
            dailyMotionUsersDownloadResult = result
            attemptToCompleteRefresh()
        }
    }
}

// MARK: Cell Models producing
extension UsersListInteractor {
    private func changeStateToLoading() {
        let loadingCellModels: [LoadingPlaceholderListCellModel] = Array(repeating: LoadingPlaceholderListCellModel(), count: 5)
        dataState = .loading(loadingCellModels)
        onCellModelsRefresh?(loadingCellModels)
    }
    
    private func changeStateToFailedToLoad() {
        let alertCellModel = AlertListCellModel(refresh: {[weak self] in
            self?.refreshData()
        })
        dataState = .failedToLoad(alertCellModel)
        onCellModelsRefresh?([alertCellModel])
    }
    
    private func changeStateToLoaded(gitHubUsers: [GitHubUser], dailyMotionUsers: [DailyMotionUser]) {
        let cellModels = makeUserCellModels(gitHubUsers: gitHubUsers, dailyMotionUsers: dailyMotionUsers)
        dataState = .loaded(cellModels)
        onCellModelsRefresh?(cellModels)
    }
    
    private func changeStateToOneOfSourcesFailedToLoad(gitHubUsers: [GitHubUser]?, dailyMotionUsers: [DailyMotionUser]?) {
        let cellModels = makeUserCellModels(gitHubUsers: gitHubUsers ?? [], dailyMotionUsers: dailyMotionUsers ?? [])
        dataState = .oneOfSourcesFailedToLoad(cellModels)
        onCellModelsRefresh?(cellModels)
    }
    
    private func makeUserCellModels(gitHubUsers: [GitHubUser], dailyMotionUsers: [DailyMotionUser]) -> [UserListCellModel] {
        let gitHubUserCellModels = gitHubUsers.map(userCellModelFromGitHubUser)
        let dailyMotionUserCellModels = dailyMotionUsers.map(userCellModelFromDailyMotionUser)
        
        return mergeCellModels(gitHubUserCellModels: gitHubUserCellModels, dailyMotionUserCellModels: dailyMotionUserCellModels)
    }
    
    private func userCellModelFromGitHubUser(_ gitHubUser: GitHubUser) -> UserListCellModel {
        return UserListCellModel(name: gitHubUser.name, avatarUrl: gitHubUser.avatarUrl, sourceImage: #imageLiteral(resourceName: "GutHub Icon"), select: { [weak self] in
            self?.handleRoute?(.showGitHubUserDetails(gitHubUser))
        })
    }
    
    private func userCellModelFromDailyMotionUser(_ dailyMotionUser: DailyMotionUser) -> UserListCellModel {
        return UserListCellModel(name: dailyMotionUser.name, avatarUrl: dailyMotionUser.avatarUrl, sourceImage: #imageLiteral(resourceName: "DailyMotion Icon"), select: { [weak self] in
            self?.handleRoute?(.showDailyMotionUserDetails(dailyMotionUser))
        })
    }
    
    private func mergeCellModels(gitHubUserCellModels: [UserListCellModel], dailyMotionUserCellModels: [UserListCellModel]) -> [UserListCellModel] {
        var cellModels: [UserListCellModel] = []
        cellModels.append(contentsOf: gitHubUserCellModels)
        cellModels.append(contentsOf: dailyMotionUserCellModels)
        cellModels.sort(by: {$0.name < $1.name})
        return cellModels
    }
}


// MARK - Routes
extension UsersListInteractor {
    enum Route {
        case showGitHubUserDetails(GitHubUser)
        case showDailyMotionUserDetails(DailyMotionUser)
    }
}
