//
//  Resource.swift
//  Users
//
//  Created by Maciej Chmielewski on 10/12/2018.
//  Copyright © 2018 Maciej Chmielewski. All rights reserved.
//

import Foundation

struct Resource<Model> {
    typealias LoadingCompletion = (Failable<Model>) -> Void
    let load: (@escaping LoadingCompletion) -> Void
}
