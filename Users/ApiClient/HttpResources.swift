//
//  HttpResources.swift
//  Users
//
//  Created by Maciej Chmielewski on 10/12/2018.
//  Copyright © 2018 Maciej Chmielewski. All rights reserved.
//

import Foundation

struct HttpResources {
    private static func httpResource<Response: Decodable>(apiRequest: ApiRequest) -> Resource<Response> {
        return Resource { completion in
            ApiClient().request(apiRequest, completion: completion)
        }
    }
}

extension HttpResources {
    static let githubUsers: Resource<[GitHubUser]> = httpResource(apiRequest: ApiRequest(url: URL(string: "https://api.github.com/users")!))
    static let dailyMotionUsers: Resource<DailyMotionUsersResponse> = httpResource(apiRequest: ApiRequest(url: URL(string: "https://api.dailymotion.com/users?fields=avatar_360_url,username")!))
}
