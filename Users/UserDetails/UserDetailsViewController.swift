//
//  UserDetailsViewController.swift
//  Users
//
//  Created by Maciej Chmielewski on 10/12/2018.
//  Copyright © 2018 Maciej Chmielewski. All rights reserved.
//

import UIKit
import AlamofireImage

class UserDetailsViewController: UIViewController {
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var coloredBackgroundView: UIView!
    @IBOutlet weak var backgroundPictureImageView: UIImageView!
    @IBOutlet weak var triangleLayerView: UIView!
    weak var traingleLayer: CALayer?
    
    var viewModel: UserDetailsViewModel!
    var onClose: (() -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        hydrateViews()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        avatarImageView.layer.cornerRadius = avatarImageView.frame.width * 0.5
        
        traingleLayer?.removeFromSuperlayer()
        traingleLayer = TriangleLayer().addLayerToView(triangleLayerView, color: viewModel.mainColor)
    }
    
    @IBAction func closeAction() {
        onClose?()
    }
    
    private func setupViews() {
        avatarImageView.layer.borderWidth = 4
        avatarImageView.layer.borderColor = UIColor.white.withAlphaComponent(0.7).cgColor
    }
    
    private func hydrateViews() {
        nameLabel.text = viewModel.name
        avatarImageView.af_setImage(withURL: viewModel.avatarUrl)
        backgroundPictureImageView.af_setImage(withURL: viewModel.avatarUrl)
        iconImageView.image = viewModel.iconImage
        coloredBackgroundView.backgroundColor = viewModel.mainColor
    }
}
