//
//  Predicates.swift
//  UsersTests
//
//  Created by Maciej Chmielewski on 10/12/2018.
//  Copyright © 2018 Maciej Chmielewski. All rights reserved.
//

import Foundation
import Nimble
@testable import Users

public func beInitial() -> Predicate<UsersListInteractor.DataState> {
    return Predicate.simple("be initial") { actualExpression in
        if case .some(.initial) = try actualExpression.evaluate() {
            return PredicateStatus(bool: true)
        } else {
            return PredicateStatus(bool: false)
        }
    }
}

public func beLoading() -> Predicate<UsersListInteractor.DataState> {
    return Predicate.simple("be loading") { actualExpression in
        if case .some(.loading) = try actualExpression.evaluate() {
            return PredicateStatus(bool: true)
        } else {
            return PredicateStatus(bool: false)
        }
    }
}

public func beLoaded() -> Predicate<UsersListInteractor.DataState> {
    return Predicate.simple("be loaded") { actualExpression in
        if case .some(.loaded) = try actualExpression.evaluate() {
            return PredicateStatus(bool: true)
        } else {
            return PredicateStatus(bool: false)
        }
    }
}


public func beOneOfSourcesFailedToLoad() -> Predicate<UsersListInteractor.DataState> {
    return Predicate.simple("be one of sources failed to load") { actualExpression in
        if case .some(.oneOfSourcesFailedToLoad) = try actualExpression.evaluate() {
            return PredicateStatus(bool: true)
        } else {
            return PredicateStatus(bool: false)
        }
    }
}

public func beFailedToLoad() -> Predicate<UsersListInteractor.DataState> {
    return Predicate.simple("be failed to load") { actualExpression in
        if case .some(.failedToLoad) = try actualExpression.evaluate() {
            return PredicateStatus(bool: true)
        } else {
            return PredicateStatus(bool: false)
        }
    }
}
