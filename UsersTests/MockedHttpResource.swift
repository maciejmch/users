//
//  MockedHttpResource.swift
//  UsersTests
//
//  Created by Maciej Chmielewski on 10/12/2018.
//  Copyright © 2018 Maciej Chmielewski. All rights reserved.
//

import Foundation
@testable import Users

class MockedHttpResource<Response> {
    private let successResponse: Response
    private var completion: Resource<Response>.LoadingCompletion?
    
    private(set) lazy var resource: Resource<Response> = {
        return Resource { [weak self] completion in
            self?.completion = completion
        }
    }()
    
    init(successResponse: Response) {
        self.successResponse = successResponse
    }
    
    func success() {
        completion?(.success(successResponse))
    }
    
    func fail() {
        completion?(.failure(MockedError()))
    }
}

extension MockedHttpResource {
    struct MockedError: Error {}
}
