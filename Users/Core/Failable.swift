//
//  Failable.swift
//  Users
//
//  Created by Maciej Chmielewski on 10/12/2018.
//  Copyright © 2018 Maciej Chmielewski. All rights reserved.
//

import Foundation

enum Failable<T> {
    case success(T)
    case failure(Error)
}

extension Failable {
    var optional: Optional<T> {
        switch self {
        case .success(let value): return value
        case .failure: return nil
        }
    }
}
