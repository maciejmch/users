//
//  UsersListInteractorTests.swift
//  UsersTests
//
//  Created by Maciej Chmielewski on 10/12/2018.
//  Copyright © 2018 Maciej Chmielewski. All rights reserved.
//

import Foundation
import XCTest
import Nimble
@testable import Users

class UsersListInteractorTests: XCTestCase {
    private func makeTestableInteractorElements() -> (interactor: UsersListInteractor, gitHubUsersResource: MockedHttpResource<[GitHubUser]>, dailyMotionUsersResource: MockedHttpResource<DailyMotionUsersResponse>, mockedInternetReconnectHandler: MockedInternetReconnectHandler) {
        let avatarUrl: URL = URL(string: "https://i.pinimg.com/originals/85/65/50/856550aa773911d00b76b24aaa4bc467.png")!
        let mockedGitHubResource = MockedHttpResource(successResponse: [GitHubUser(name: "github user", avatarUrl: avatarUrl)])
        let mockedDailyMotionResource = MockedHttpResource(successResponse: DailyMotionUsersResponse(users: [DailyMotionUser(name: "dailymotion user", avatarUrl: avatarUrl)]))
        let mockedInternetReconnectHnalder = MockedInternetReconnectHandler()
        
        let interactor = UsersListInteractor(gitHubUsersResource: mockedGitHubResource.resource,
                                             dailyMotionUsersResource: mockedDailyMotionResource.resource,
                                             internetReconnectHandler: mockedInternetReconnectHnalder)
        return (interactor, mockedGitHubResource, mockedDailyMotionResource, mockedInternetReconnectHnalder)
    }
    
    func testUsersLoadingHappyPath() {
        let (interactor, gitHubResource, dailyMotionResource, _) = makeTestableInteractorElements()
        expect(interactor.dataState).to(beInitial())
        
        interactor.refreshData()
        expect(interactor.dataState).to(beLoading())
        
        gitHubResource.success()
        dailyMotionResource.success()
        
        expect(interactor.dataState).to(beLoaded())
    }
    
    func testGitHubUsersDownloadFail() {
        let (interactor, gitHubResource, dailyMotionResource, _) = makeTestableInteractorElements()
        interactor.refreshData()
        expect(interactor.dataState).to(beLoading())
        
        gitHubResource.fail()
        dailyMotionResource.success()
        
        expect(interactor.dataState).to(beOneOfSourcesFailedToLoad())
    }
    
    func testDailyMotionUsersDownloadFail() {
        let (interactor, gitHubResource, dailyMotionResource, _) = makeTestableInteractorElements()
        interactor.refreshData()
        expect(interactor.dataState).to(beLoading())
        
        gitHubResource.success()
        dailyMotionResource.fail()
        expect(interactor.dataState).to(beOneOfSourcesFailedToLoad())
    }
    
    func testBothDownloadsFail() {
        let (interactor, gitHubResource, dailyMotionResource, _) = makeTestableInteractorElements()
        interactor.refreshData()
        expect(interactor.dataState).to(beLoading())
        
        gitHubResource.fail()
        dailyMotionResource.fail()
        expect(interactor.dataState).to(beFailedToLoad())
    }
    
    func testWaitingForCompleteDownload() {
        let (interactor, gitHubResource, dailyMotionResource, _) = makeTestableInteractorElements()
        interactor.refreshData()
        expect(interactor.dataState).to(beLoading())
        
        gitHubResource.success()
        expect(interactor.dataState).to(beLoading())
        
        dailyMotionResource.success()
        expect(interactor.dataState).to(beLoaded())
    }
    
    func testReconnect() {
        let (interactor, gitHubResource, dailyMotionResource, reconnectHandler) = makeTestableInteractorElements()
        interactor.refreshData()
        
        expect(interactor.dataState).to(beLoading())
        
        gitHubResource.fail()
        dailyMotionResource.fail()
        expect(interactor.dataState).to(beFailedToLoad())
        
        reconnectHandler.doReconnect()
        expect(interactor.dataState).to(beLoading())
    }
    
    func testRefreshDataWhenFailed() {
        let (interactor, gitHubResource, dailyMotionResource, _) = makeTestableInteractorElements()
        interactor.refreshData()
        expect(interactor.dataState).to(beLoading())
        
        gitHubResource.fail()
        dailyMotionResource.fail()
        expect(interactor.dataState).to(beFailedToLoad())
        
        interactor.refreshData()
        expect(interactor.dataState).to(beLoading())
    }
    
    func testRefreshDataWhenSucceeded() {
        let (interactor, gitHubResource, dailyMotionResource, _) = makeTestableInteractorElements()
        interactor.refreshData()
        
        expect(interactor.dataState).to(beLoading())
        
        gitHubResource.success()
        dailyMotionResource.success()
        expect(interactor.dataState).to(beLoaded())
        
        interactor.refreshData()
        expect(interactor.dataState).to(beLoaded())
    }
    
    func testRefreshDataWhenOneOfSourcesFailed() {
        let (interactor, gitHubResource, dailyMotionResource, _) = makeTestableInteractorElements()
        interactor.refreshData()
        
        expect(interactor.dataState).to(beLoading())
        
        gitHubResource.success()
        dailyMotionResource.fail()
        expect(interactor.dataState).to(beOneOfSourcesFailedToLoad())
        
        interactor.refreshData()
        expect(interactor.dataState).to(beOneOfSourcesFailedToLoad())
    }
}
