//
//  LoadingPlaceholderListCell.swift
//  Users
//
//  Created by Maciej Chmielewski on 10/12/2018.
//  Copyright © 2018 Maciej Chmielewski. All rights reserved.
//

import UIKit

class LoadingPlaceholderListCell: UITableViewCell {
    @IBOutlet weak var gradientImageView: UIImageView!
    private var animate = false
    
    func startAnimating() {
        animate = true
        gradientImageView.frame = CGRect(origin: CGPoint(x: -gradientImageView.frame.size.width, y: 0), size: gradientImageView.frame.size)
        
        UIView.animate(withDuration: 1.4, animations: { [weak self] in
            guard let `self` = self else {return}
            self.gradientImageView.frame = CGRect(origin: CGPoint(x: self.frame.width, y: 0), size: self.gradientImageView.frame.size)
        }) {[weak self] _ in
            guard let `self` = self else {return}
            if self.animate {
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.7) {
                    self.startAnimating()
                }
            }
        }
    }
    
    func stopAnimating() {
        animate = false
    }
}
