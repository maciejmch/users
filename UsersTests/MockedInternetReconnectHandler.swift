//
//  MockedInternetReconnectHandler.swift
//  UsersTests
//
//  Created by Maciej Chmielewski on 10/12/2018.
//  Copyright © 2018 Maciej Chmielewski. All rights reserved.
//

import Foundation
@testable import Users

class MockedInternetReconnectHandler: InternetReconnectHandler {
    var onReconnect: (() -> Void)?
    
    func doReconnect()  {
        onReconnect?()
    }
}
